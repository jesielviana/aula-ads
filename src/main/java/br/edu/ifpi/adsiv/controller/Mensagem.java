package br.edu.ifpi.adsiv.controller;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;


public class Mensagem {
	
	@Inject
	private FacesContext facesContext;
	
	protected void addSucesso(String msg){
		FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso!", msg);
		facesContext.addMessage(null, m);
	}
	
	protected void addErro(String msg){
		FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", msg);
		facesContext.addMessage(null, m);
	}

}
