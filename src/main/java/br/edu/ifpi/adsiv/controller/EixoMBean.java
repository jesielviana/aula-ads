package br.edu.ifpi.adsiv.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Produces;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.edu.ifpi.adsiv.model.Eixo;
import br.edu.ifpi.adsiv.service.EixoService;

@ViewScoped
@Named
public class EixoMBean extends Mensagem implements Serializable {

	private static final long serialVersionUID = 1L;
	@Inject
	private EixoService eixoService;
	@Produces
	@Named
	private List<Eixo> eixos;
	@Produces
	@Named
	private Eixo newEixo;

	private boolean altera;

	@PostConstruct
	public void init() {
		iniciaNovoEixo();
		carregaTodosEixos();
	}

	/*
	 * 
	 */
	private void iniciaNovoEixo() {
		newEixo = new Eixo();
		this.altera = false;

	}

	/**
	 * 
	 */
	public void carregaTodosEixos() {
		try {
			this.eixos = eixoService.listaTodos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void adiciona() {
		try {
			eixoService.adiciona(newEixo);
			this.eixos.add(newEixo);
			iniciaNovoEixo();
			super.addSucesso("Eixo inserido com sucesso");
		} catch (Exception e) {
			super.addErro("Erro ao inserir eixo.");
			e.printStackTrace();
		}

	}


	/**
	 * 
	 * @param eixo
	 */
	public void remove(Eixo eixo) {
		try {
			eixoService.remove(eixo);
			addSucesso("Eixo removido com sucesso.");
			carregaTodosEixos();
		} catch (Exception e) {
			addErro("Erro ao remover eixo.");
		}

	}

	public void carregaEixoParaAlterar(Eixo eixo) {
		newEixo = eixo;
		this.altera = true;
	}

	/**
	 * 
	 */
	public void atualiza() {
		try {
			eixoService.atualiza(newEixo);
			addSucesso("Eixo atualizado com sucesso.");
			iniciaNovoEixo();
			carregaTodosEixos();
		} catch (Exception e) {
			addErro("Erro ao atualizar eixo.");
		}

	}

	public boolean isAltera() {
		return altera;
	}

}
