package br.edu.ifpi.adsiv.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.edu.ifpi.adsiv.model.Eixo;
import br.edu.ifpi.adsiv.model.Professor;
import br.edu.ifpi.adsiv.service.EixoService;
import br.edu.ifpi.adsiv.service.ProfessorService;

@ViewScoped
@Named
public class ProfessorMBean extends Mensagem{
	
	@Inject
	private ProfessorService service;
	private Professor newProfessor;
	@Inject
	private EixoService eixoService;
	private Integer idEixoSelecionado;
	private List<Eixo> eixos;
	private List<Professor> professores;
	
	@PostConstruct
	public void init(){
		iniciaNoveProfessor();
		carregaEixos();
		carregaProfessores();
	}
	
	private void iniciaNoveProfessor(){
		newProfessor = new Professor();
	}
	
	private void carregaEixos(){
		try {
			eixos = eixoService.listaTodos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void carregaProfessores(){
		professores = service.listaTodos();
	}
	
	public void adicionarProfessor(){
		service.adiciona(newProfessor, idEixoSelecionado);
		addSucesso("Professor adicionado com sucesso.");
	}

	public Professor getNewProfessor() {
		return newProfessor;
	}

	public void setNewProfessor(Professor newProfessor) {
		this.newProfessor = newProfessor;
	}

	public Integer getIdEixoSelecionado() {
		return idEixoSelecionado;
	}

	public void setIdEixoSelecionado(Integer idEixoSelecionado) {
		this.idEixoSelecionado = idEixoSelecionado;
	}

	public List<Eixo> getEixos() {
		return eixos;
	}

	public void setEixos(List<Eixo> eixos) {
		this.eixos = eixos;
	}
	
	public List<Professor> getProfessores() {
		return professores;
	}

}
