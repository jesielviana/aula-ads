package br.edu.ifpi.adsiv.controller;

import javax.enterprise.inject.Model;

/**
 * 
 * @author jesielviana
 *
 */
@Model
public class LoginController {

	private String email;
	private String senha;

	public String login() {
		System.out.println("Email: "+email);
		System.out.println("Senha: "+senha);
		return "eixo";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
