package br.edu.ifpi.adsiv.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

public abstract class CrudDao<T extends Serializable> implements ICrudDao<T> {

	@Inject
	private EntityManager entityManager;

	private final Class<T> classe;

	public CrudDao(Class<T> classe) {
		this.classe = classe;
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public void adiciona(T entidade) {
		entityManager.persist(entidade);
	}

	@Override
	public void remove(T entidade) {
		entityManager.remove(entidade);
	}

	@Override
	public void atualiza(T entidade) {
		entityManager.merge(entidade);
	}

	@Override
	public T buscaPorId(int id) {
		return entityManager.find(classe, id);
	}

	public List<T> listaTodos() {
		CriteriaQuery<T> query = entityManager.getCriteriaBuilder().createQuery(classe);
		query.from(classe);
		return entityManager.createQuery(query).getResultList();
	}

}
