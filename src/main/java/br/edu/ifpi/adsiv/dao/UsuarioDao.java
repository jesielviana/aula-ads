package br.edu.ifpi.adsiv.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.edu.ifpi.adsiv.model.Usuario;

public class UsuarioDao implements ICrudDao<Usuario> {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public void adiciona(Usuario entidade) {
		em.persist(entidade);
	}

	@Override
	public void remove(Usuario entidade) {
		em.remove(entidade);
	}

	@Override
	public void atualiza(Usuario entidade) {
		em.merge(entidade);
	}

	@Override
	public Usuario buscaPorId(int id) {
		return em.find(Usuario.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> listaTodos() {
		Query query = em.createQuery("select u from Usuario as u Order by u.id");
		return query.getResultList();
	}


	

}
