package br.edu.ifpi.adsiv.dao;

import java.io.Serializable;
import java.util.List;

public interface ICrudDao <T extends Serializable> {
	
	public void adiciona(T entidade);
	
	public void remove(T entidade);

	public void atualiza(T entidade);

	public T buscaPorId(int id);
	
	public List<T> listaTodos();

}
