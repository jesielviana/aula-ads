package br.edu.ifpi.adsiv.dao;

import br.edu.ifpi.adsiv.model.Coordenador;

/**
 * 
 * @author jesielviana
 *
 */
public class CoordenadorDao extends CrudDao<Coordenador> {

	public CoordenadorDao() {
		super(Coordenador.class);
	}


}
