package br.edu.ifpi.adsiv.dao;

import java.util.List;

import javax.inject.Named;
import javax.persistence.Query;

import br.edu.ifpi.adsiv.model.Eixo;

/**
 * 
 * @author jesielviana
 *
 */
@Named
public class EixoDao extends CrudDao<Eixo> {

	public EixoDao() {
		super(Eixo.class);
	}

	
	@SuppressWarnings("unchecked")
	public List<Eixo> buscaTodos(){
		Query query = getEntityManager().createQuery("select e from Eixo as e Order by e.nome");
		return query.getResultList();
	}
	
	
	@Override
	public void remove(Eixo entidade) {
		Query query = getEntityManager().createQuery("DELETE FROM Eixo AS e Where e.id = :id");
		query.setParameter("id", entidade.getId());
		query.executeUpdate();
	}
}
