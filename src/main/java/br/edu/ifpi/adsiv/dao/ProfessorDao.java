package br.edu.ifpi.adsiv.dao;

import br.edu.ifpi.adsiv.model.Professor;

public class ProfessorDao extends CrudDao<Professor> {

	public ProfessorDao() {
		super(Professor.class);
	}

}
