package br.edu.ifpi.adsiv.dao;

import br.edu.ifpi.adsiv.model.Curso;

public class CursoDao extends CrudDao<Curso> {

	public CursoDao() {
		super(Curso.class);
	}

}
