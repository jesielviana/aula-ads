package br.edu.ifpi.adsiv.service;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.edu.ifpi.adsiv.model.Eixo;
import br.edu.ifpi.adsiv.model.Professor;

@Singleton
//@Startup
public class UsuarioService {

	@PersistenceContext
	private EntityManager em;

		
	//@PostConstruct
	public void adiciona() {
		Eixo e = new Eixo();
		e.setNome("Tecnolgia");
		em.persist(e);
		
		Professor p = new Professor();
		p.setNome("Jesiel");
		p.setEixo(e);
		em.persist(p);


	}

}
