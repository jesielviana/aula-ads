package br.edu.ifpi.adsiv.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.edu.ifpi.adsiv.dao.EixoDao;
import br.edu.ifpi.adsiv.dao.ProfessorDao;
import br.edu.ifpi.adsiv.model.Eixo;
import br.edu.ifpi.adsiv.model.Professor;

@Stateless
public class ProfessorService {

	@Inject
	private EixoDao eixoDao;
	@Inject
	private ProfessorDao professorDao;
	
	public void adiciona(Professor professor, Integer idEixo){
		Eixo e = eixoDao.buscaPorId(idEixo);
		professor.setEixo(e);
		professorDao.adiciona(professor);
		
	}
	
	
	public List<Professor> listaTodos(){
		return professorDao.listaTodos();
	}
}
