package br.edu.ifpi.adsiv.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.edu.ifpi.adsiv.dao.EixoDao;
import br.edu.ifpi.adsiv.model.Eixo;

@Stateless
public class EixoService {

	@Inject
	private EixoDao eixoDao;
	@Inject
	private Logger log;

	public void adiciona(Eixo eixo) throws Exception {
		eixoDao.adiciona(eixo);
		log.info("Adiocionando eixo: " + eixo.getNome());
	}

	public void atualiza(Eixo eixo) throws Exception {
		eixoDao.atualiza(eixo);
	}

	public void remove(Eixo eixo) throws Exception {
		eixoDao.remove(eixo);
	}

	public List<Eixo> listaTodos() throws Exception {
		return eixoDao.buscaTodos();
	}

}
