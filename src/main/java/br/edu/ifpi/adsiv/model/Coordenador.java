package br.edu.ifpi.adsiv.model;

import javax.persistence.Entity;

@SuppressWarnings("serial")
@Entity
public class Coordenador extends Professor {
	
	public Coordenador() {
	}
	
	
	public Coordenador(Professor professor) {
		setId(professor.getId());
		setNome(professor.getNome());
		setEmail(professor.getEmail());
		setSiape(professor.getSiape());
		setEixo(professor.getEixo());
	}
	

}
