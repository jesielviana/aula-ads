package br.edu.ifpi.adsiv.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Entity
public class Curso implements Serializable {
	@Id
	@GeneratedValue
	private int id;
	private String nome;
	private int modulos;
	private int duracaoMin;
	private int duracaoMax;
	@ManyToOne
	private Coordenador coordenador;
	@ManyToOne
	private Eixo eixo;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getModulos() {
		return modulos;
	}
	public void setModulos(int modulos) {
		this.modulos = modulos;
	}
	public int getDuracaoMin() {
		return duracaoMin;
	}
	public void setDuracaoMin(int duracaoMin) {
		this.duracaoMin = duracaoMin;
	}
	public int getDuracaoMax() {
		return duracaoMax;
	}
	public void setDuracaoMax(int duracaoMax) {
		this.duracaoMax = duracaoMax;
	}
	public Coordenador getCoordenador() {
		return coordenador;
	}
	public void setCoordenador(Coordenador coordenador) {
		this.coordenador = coordenador;
	}
	public Eixo getEixo() {
		return eixo;
	}
	public void setEixo(Eixo eixo) {
		this.eixo = eixo;
	}
	
	

}
